name = Slideshow CS
description = A slideshow block with cool effects using jQuery plugin: Cross-slide
core = 6.x
dependencies[] = jq
dependencies[] = jquery_update
dependencies[] = jquery_plugin
package = "User interface"